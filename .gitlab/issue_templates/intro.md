(please erase all stuff in parentheses before you submit. 

Please note that all issues on this repo are public so you should not put anything here that you would not want seen by the public. You must put a name for the title of your issue template but it does not have to be your full name or even a real name, as long as it is a functional name that you will expect people to know and use on the git repo and on the slack channel. Generally speaking you do not have to fear going by a first name or something of that sort but if you would feel more comfortable not doing that, that is understandable. We are trying to respect people's privacy while also insuring we do not create unnecessary friction through obfuscation. 

also please add to the title in parentheses a general job title so that people can get a sense of what skills you offer at a glance. 

You can see an example of a completed issue here: https://gitlab.com/dsausa/chapters/austin/techcomm-onboarding/-/issues/2) 

## Intro
(quick summary of who you are. Can include name, pronouns, your technical/professional background, why you joined Austin DSA, and/or why you joined NTC)

### What I know
(quick summary of your skills/experience/expertise/interests)

### What I want to know
(quick summary of things you want to learn more about, or are learning more about.)

### Personal projects of note
(list here any personal open source projects of note that you want to promote or that might provide some context for your skillset in a more concrete fashion)


## contact information
(to help increase communication on projects, please list any contact information that will help people ask you questions etc. People can also directly contact you on this issue but not everyone checks gitlab or has notifications set up)

## Project Involvement and Roles
(this section might not be filled out until later but this is where you come to update your involvement in specific DSA projects. Doing this will make it easier for people to know who is working on what. 
)

- please list 
- project names
- using bullets like this


(below are some automatically applied actions. Please add your skillsets as labels using the user interface)

/label ~intro ~new
/cc @jcklpe @gwashburn 
/assign me
/subscribe
